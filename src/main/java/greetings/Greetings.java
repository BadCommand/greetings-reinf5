package greetings;

import java.util.Scanner;

public class Greetings {
	
	public static String getGreeting(String city) {
		// TODO: Your code goes here
		
		String[] cities = {"Berlin", "Bern", "London", "Boston", "Paris", "Milano", "Madrid"};
		String[] language = {"Guten Tag!", "Guten Tag!", "Hello!", "Hello!", "Bonjour!", "Ciao!", "Hola!"};
		int i = 0;
		String output = "City not found!";
		
		while (i <= 6) {
			
			if (city.toLowerCase().equals(cities[i].toLowerCase())) {
				output = language[i];
				break;
				
			}
			i++;
		}
		
		
		return output;
	}
	
  public static void main(String[] args) {
	  
	  Scanner scanner = new Scanner(System.in);
	  System.out.println("Enter a city: ");
	  String city = scanner.nextLine();
	  System.out.println(getGreeting(city));
	  scanner.close();
	  

  }
}

